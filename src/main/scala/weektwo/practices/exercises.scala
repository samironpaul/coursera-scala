package weektwo.practices

/**
  * Created by samir on 8/28/2016.
  */
object exercises {
  def product(f: Int => Int)(a: Int, b: Int): Int = {
    if(a > b) 1
    else f(a) * product(f)(a+1, b)
  }

}
