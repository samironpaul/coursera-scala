"========= week 2 ==========="
"... Currying ..."

def sum1(i: Int, j: Int, k: Int): Int = i+j+k
sum1(10,20,30)

// Redefining sum1.
// Singling out the last parameter.
// So that sum2 can be used as an independent function
def sum2(i: Int, j: Int): Int => Int = {
  def innerSum(k: Int): Int = {
    i+j+k
  }
  innerSum
}
sum2(10,20)(30)
def sumFunc = sum2(10,20)
sumFunc(30)

/** Sum2 is an example of basic currying conception.
  * Now applying the syntactic sugar.
  */
def sum3(i: Int, j: Int)(k: Int): Int = {
  i+j+k
}
sum3(10,20)(30)
def sumFunc3_firstTwoArgs = sum3(10,20)_
sumFunc3_firstTwoArgs(30)

/**
  * Same currying demonstration above shown with coursera
  * example
  */
// Regular sum function
def sumE(f: Int => Int, a: Int, b: Int): Int = {
  if(a > b) 0
  else f(a) + sumE(f, a+1, b)
}
def sumInts(a: Int, b: Int) = sumE(x => x, a, b)
sumInts(1, 3)

def sumE2(f: Int => Int): (Int, Int) => Int = {
  def innerSumE(a: Int, b: Int): Int = {
    if(a > b) 0
    else f(a) + innerSumE(a+1, b)
  }
  innerSumE
}
def sumInts2 = sumE2(x => x)
sumInts2(1, 3)

sumE2(x=>x)(1,4)

def sumE3(f: Int => Int)(a: Int, b: Int): Int = {
  if(a > b) 0
  else f(a) + sumE3(f)(a+1, b)
}
def something = sumE3(x=>x)_
something(1, 5)




/**
  * Currying exercises based on product function
  */
def product(f: Int => Int)(a: Int, b: Int): Int = {
  if(a > b) 1
  else f(a) * product(f)(a+1, b)
}
def multiSqr = product(x => x * x) _
multiSqr(2,3)

def factorial(n: Int) = product(x => x)(2, n)
factorial(9)

def genericFunction(sequenceFunc: (Int, Int) => Int)(unitValue: Int)(numberFunc: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) unitValue
  else sequenceFunc(numberFunc(a), genericFunction(sequenceFunc)(unitValue)(numberFunc)(a+1, b))
}
def genericProduct(a: Int, b: Int) = genericFunction((a, b) => a * b)(1)(x => x) (a, b)
genericProduct(2, 10)
def genericFact(a: Int) = genericProduct(1, a)
genericFact(10)
def genericSummation(a: Int, b: Int) = genericFunction((a,b) => a + b)(0)(x=>x)(a, b)
genericSummation(1, 10)


