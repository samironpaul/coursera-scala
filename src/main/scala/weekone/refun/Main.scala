package weekone.refun

import scala.annotation.tailrec
import scala.collection.mutable.Stack

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    val max = 10
    for (row <- 0 to max) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }

    println("Parenthesis balance check")
    println(balance("pas(r-19[](),c-1)".toList))
    println(balance("(if (zero? x) max (/ 1 x))".toList))
    println(balance("I told him (that it’s not (yet) done). (But he wasn’t listening)".toList))
    println(balance(":-)".toList))
    println(balance("())(".toList))

    println(countChange(300, List(5, 10, 20, 50, 100, 200, 500)))
  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {
    def matches(c1: Char, c2: Char): Boolean = {
      if (c1 == '(' && c2 == ')') true
      else if (c1 == '{' && c2 == '}') true
      else if (c1 == '[' && c2 == ']') true
      else false
    }
    def isRightBrace(c: Char): Boolean = {
      if (c == ')' || c == '}' || c == ']') true
      else false
    }
    def isLeftBrace(c: Char): Boolean = {
      if (c == '(' || c == '{' || c == '[') true
      else false
    }

    @tailrec
    def checkBalance(str: List[Char], stack: Stack[Char]): Boolean = {
      if (str.isEmpty) stack.isEmpty
      else {
        val c = str.head
        if (isLeftBrace(c)) {
          stack.push(c)
        } else if (isRightBrace(c)) {
          if (stack.isEmpty) return false
          else if (!matches(stack.pop(), c)) return false
        }
        checkBalance(str.tail, stack)
      }
    }

    checkBalance(chars, Stack())
  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {
    val cs = coins.sorted
    val cEnd = cs.length - 1
    def recursiveCount(m: Int, coinIndex: Int): Int = {
      if (m == 0) return 1
      else {
        var total = 0;
        for (i <- coinIndex to cEnd) {
          if (m - cs(i) == 0) total = total + 1
          else if (m - cs(i) > 0) total = total + recursiveCount(m - cs(i), i)
          else 0
        }
        total
      }
    }
    recursiveCount(money, 0);
  }
}
